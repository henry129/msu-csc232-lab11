/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017
 *
 * @file Palindrominator.cpp
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          Jonah Henry <henry129@live.missouristate.edu>
 * @brief   Palindrominator implementation.
 */
#include "Palindrominator.h"
#include <cstdlib>
#include <queue>
#include <stack>
#include <iostream>

bool Palindrominator::isPalindrome(const std::string text) {
    std::stack<int> Stack;
    std::queue<int> Queue;

    for (int i=0; i<text.size(); i++){
        Queue.push(text[i]);
    }
	for (int i=0; i<text.size(); i++){
            Stack.push(text[i]);
	}

	while (!Queue.empty()) {
       if(Queue.front() != Stack.top()){
           return false;
       }
       else{
           Queue.pop();
           Stack.pop();
       }
    }
    return true;
}
